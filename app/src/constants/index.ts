export const IS_PRODUCTION = process.env.NODE_ENV === "production";
export const IS_DEVELOPMENT = process.env.NODE_ENV === "development";

export const APP_NAME = "Portfolio";
export const DESCRIPTION = "Portfolio";
export const API_URL = "http://dmitry-portfolio.ru:8000/api/project/";
