export const HOME = "/";
export const ERROR_404 = "/404";
export const PORTFOLIO = "portfolio";
export const PORTFOLIO_ITEM = "/portfolio/:id";
export const CONTACT = "contact";
