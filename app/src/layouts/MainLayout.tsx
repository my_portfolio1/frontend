import { FC } from "react";
import { Outlet } from "react-router-dom";
import Preloader from "~/components/UI/Preloader";
import UserInfo from "~/components/UserInfo";
import Nav from "~/components/UI/Nav";
import Footer from "~/components/Footer";

const MainLayout: FC = () => {
  return (
    <>
      <Preloader active={false} />
      <main className="main">
        <div className="container">
          <div className="main__inner">
            <div className="main__user--info">
              <UserInfo />
            </div>
            <div className="main__main">
              <Nav />
              <Outlet />
              <Footer />
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default MainLayout;
