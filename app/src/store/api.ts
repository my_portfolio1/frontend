import { EndpointBuilder } from "@reduxjs/toolkit/dist/query/endpointDefinitions";
import { emptySplitApi as api } from "./emptyApi";

const injectedRtkApi = api.injectEndpoints({
  endpoints: (build: EndpointBuilder<any, any, any>) => ({
    projectsList: build.query<ProjectsListApiResponse, ProjectsListApiArg>({
      query: () => ({ url: "/projects/" }),
    }),
    projectsRead: build.query<ProjectsReadApiResponse, ProjectsReadApiArg>({
      query: (queryArg) => ({ url: `/projects/${queryArg.id}/` }),
    }),
  }),
  overrideExisting: false,
});
export { injectedRtkApi as api };
export type ProjectsListApiResponse = /** status 200  */ ProjectProjectList[];
export type ProjectsListApiArg = void;
export type ProjectsReadApiResponse = /** status 200  */ ProjectProjectList;
export type ProjectsReadApiArg = {
  id: string;
};
export type TypeTypeRead = {
  id?: number;
  name?: "frontend" | "backend" | "devops" | "design";
};
export type ProjectProjectList = {
  id?: number;
  title?: string;
  avatar?: string;
  types?: TypeTypeRead[];
  description?: string;
  link_repo?: string;
  link_project?: string;
  technologies?: string;
  tools?: string;
};
export const { useProjectsListQuery, useProjectsReadQuery } = injectedRtkApi;
