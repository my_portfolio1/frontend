import { configureStore } from "@reduxjs/toolkit";
import { emptySplitApi as api } from "./emptyApi";

const store = configureStore({
  reducer: {
    [api.reducerPath]: api.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(api.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
// @ts-ignore
export type AppStore = ReturnType<typeof store>;
export type AppDispatch = AppStore["dispatch"];

export default store;
