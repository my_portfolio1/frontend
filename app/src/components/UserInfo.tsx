import { FC, memo } from "react";
import Avatar from "~/components/UI/User/Avatar";
import UserName from "~/components/UI/User/UserName";
import EyeGlasses from "~/components/UI/svg/EyeGlasses";
import UserInfoItem from "~/components/UI/User/UserInfoItem";

const UserInfo: FC = () => {
  return (
    <div className="user__info" id="info">
      <Avatar />
      <UserName username="Дмитрий Герасимов" specialization="Веб разработчик" />

      <div className="user__info--info">
        <header className="user_info--circle">
          <EyeGlasses />
        </header>
        <ul className="user__info--items">
          <li className="user__info--item-first" />
          <UserInfoItem name="Имя" value="Дмитрий" />
          <UserInfoItem name="Gitlab" value="@dmitryger" />
          <UserInfoItem name="Специальность" value="Веб-разработчик" />
          <UserInfoItem name="Почта" value="dmitry-dev.front@yandex.ru" />
        </ul>
      </div>
    </div>
  );
};

export default memo(UserInfo);
