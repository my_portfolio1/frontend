import { FC, memo } from "react";

const Footer: FC = () => {
  return (
    <footer className="footer">
      <span className="footer__author">Дмитрий Герасимов</span>
      <span className="footer__copyright">© 2021 All rights reserved.</span>
    </footer>
  );
};

export default memo(Footer);
