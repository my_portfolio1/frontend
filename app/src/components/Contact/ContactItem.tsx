import { FC, ReactNode } from "react";

interface Props {
  title: string;
  children: ReactNode;
  text: string;
  link: string;
}

const ContactItem: FC<Props> = ({ children, title, text, link }) => {
  return (
    <div className="contact__body--item">
      {children}
      <h4 className="home__body--title">{title}</h4>
      <p className="home__body--text">
        <a href={link} className="contact__email">
          {text}
        </a>
      </p>
    </div>
  );
};

export default ContactItem;
