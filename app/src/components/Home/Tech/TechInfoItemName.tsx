import { FC, memo } from "react";

interface Props {
  name: string;
  text: string;
}

const TechInfoItemName: FC<Props> = ({ name, text }) => {
  return (
    <p className="user__info--item-name-p">
      <span className="user__info--item-name">{name}:</span>
      {text}
    </p>
  );
};

export default memo(TechInfoItemName);
