import { FC, ReactNode } from "react";

interface Props {
  children: ReactNode;
  title: string;
}

const TechInfoItem: FC<Props> = ({ children, title }) => {
  return (
    <li className="tech__info">
      <h3 className="tech__info--title">{title}</h3>
      {children}
    </li>
  );
};

export default TechInfoItem;
