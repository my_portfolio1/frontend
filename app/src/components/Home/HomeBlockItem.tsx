import { FC, ReactNode } from "react";

interface Props {
  children?: ReactNode;
  title: string;
  text: string;
}

const HomeBlockItem: FC<Props> = ({ children, title, text }) => {
  return (
    <div className="home__body--item">
      {children}
      <h4 className="home__body--title">{title}</h4>
      <p className="home__body--text">{text}</p>
    </div>
  );
};

export default HomeBlockItem;
