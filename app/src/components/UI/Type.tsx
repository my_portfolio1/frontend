import { FC, memo } from "react";

interface Props {
  name?: string;
}

const TypeModal: FC<Props> = ({ name }) => {
  return <span className="type_item">{name}</span>;
};

export default memo(TypeModal);
