import { FC, memo } from "react";
import clsx from "clsx";
import styles from "~/styles/modules/Skeleton.module.scss";

interface Props {
  card?: boolean;
  text?: boolean;
  btn?: boolean;
  title?: boolean;
  image?: boolean;
  type?: boolean;
}

const Skeleton: FC<Props> = ({ card, text, btn, type, title, image }) => {
  return (
    <span
      className={clsx(styles.Skeleton, styles.SkeletonWave, {
        [styles.SkeletonCard]: card,
        [styles.SkeletonText]: text,
        [styles.SkeletonBtn]: btn,
        [styles.SkeletonImage]: image,
        [styles.SkeletonTitle]: title,
        [styles.SkeletonType]: type,
      })}
    />
  );
};

export default memo(Skeleton);
