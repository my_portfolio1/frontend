import { FC, memo } from "react";
import Skeleton from "~/components/UI/skeleton/Skeleton";

const SkeletonCard: FC = () => {
  return (
    <div className="portfolio__card">
      <div className="bg__scale">
        <Skeleton card />
      </div>

      <div className="portfolio__card-inner">
        <div className="project__name">
          <Skeleton text />
        </div>
      </div>
    </div>
  );
};

export default memo(SkeletonCard);
