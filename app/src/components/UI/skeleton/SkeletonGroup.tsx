import { FC } from "react";
import SkeletonCard from "~/components/UI/skeleton/SkeletonCard";
import Skeleton from "~/components/UI/skeleton/Skeleton";

const SkeletonGroupCard: FC = () => {
  return (
    <>
      {[...Array(3)].map((e, i) => {
        // eslint-disable-next-line react/no-array-index-key
        return <SkeletonCard key={`${e}_${i}`} />;
      })}
    </>
  );
};

export const SkeletonModal: FC = () => {
  return (
    <>
      <header className="modal__header">
        <div className="modal__header--info">
          <div className="modal__name">
            <Skeleton title />
          </div>
        </div>
        <div className="modal__header--btn">
          <Skeleton btn />
        </div>
      </header>
      <main className="modal__main">
        <div className="modal__main--left">
          <div className="modal__slider--photos">
            <Skeleton image />
          </div>
        </div>
        <div className="modal__main--right">
          <div className="modal__types">
            <Skeleton type />
          </div>
          <div className="modal__main--content">
            <h3 className="modal__main--header">
              <Skeleton title />
            </h3>
            <p className="modal__main--text">
              <Skeleton text />
            </p>
          </div>
        </div>
      </main>
    </>
  );
};

export default SkeletonGroupCard;
