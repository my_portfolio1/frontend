import { FC, ReactNode } from "react";
import clsx from "clsx";

interface Props {
  children: ReactNode;
  title: string;
  darkBackground?: boolean;
  items?: boolean;
  position?: boolean;
}

const PageBlock: FC<Props> = ({
  children,
  title,
  darkBackground,
  position,
  items,
}) => {
  return (
    <section className={clsx("home__about", { bg__grey: darkBackground })}>
      <div className="home__about--padding">
        <h3 className="home__title">
          <span className="title__line" />
          <span>{title}</span>
        </h3>
        <main
          className={clsx("home__body", {
            "home__body-items": items,
            contact__justify: position,
          })}
        >
          {children}
        </main>
      </div>
    </section>
  );
};

export default PageBlock;
