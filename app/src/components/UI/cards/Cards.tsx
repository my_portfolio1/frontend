import { FC } from "react";
import CardItem from "~/components/UI/cards/CardItem";
import SkeletonGroupCard from "~/components/UI/skeleton/SkeletonGroup";
import { useProjectsListQuery } from "~/store/api";

const Cards: FC = () => {
  const { data, isLoading } = useProjectsListQuery();

  return (
    <div className="portfolio__cards">
      {/* eslint-disable-next-line no-nested-ternary */}
      {isLoading ? (
        <SkeletonGroupCard />
      ) : data !== undefined ? (
        data.map((item) => <CardItem key={item.id} card={item} />)
      ) : (
        ""
      )}
    </div>
  );
};

export default Cards;
