import { FC } from "react";

import { useNavigate } from "react-router-dom";
import { ProjectProjectList } from "~/store/api";

interface Props {
  card: ProjectProjectList;
}

const CardItem: FC<Props> = ({ card }) => {
  const navigate = useNavigate();
  const openProject = () => {
    navigate(`/portfolio/${card.id}`);
  };

  return (
    <button type="button" className="portfolio__card" onClick={openProject}>
      <div className="bg__scale">
        <img src={card.avatar} className="portfolio__card--photo" alt="" />
      </div>

      <div className="portfolio__card-inner">
        <div className="project__name">{card.title}</div>
      </div>
    </button>
  );
};

export default CardItem;
