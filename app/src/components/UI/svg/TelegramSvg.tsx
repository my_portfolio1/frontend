import { FC, memo } from "react";

const TelegramSvg: FC = () => {
  return (
    <svg
      className="symbol__telegram"
      id="telegram"
      width="12"
      height="12"
      viewBox="0 0 12 12"
    >
      <g>
        <g clipPath="url(#clip0)">
          <path
            d="M4.70847 7.5905L4.50997 10.3825C4.79397 10.3825 4.91697 10.2605 5.06447 10.114L6.39597 8.8415L9.15497 10.862C9.66097 11.144 10.0175 10.9955 10.154 10.3965L11.965 1.9105L11.9655 1.91C12.126 1.162 11.695 0.869504 11.202 1.053L0.556967 5.1285C-0.169533 5.4105 -0.158533 5.8155 0.433467 5.999L3.15497 6.8455L9.47647 2.89C9.77397 2.693 10.0445 2.802 9.82197 2.999L4.70847 7.5905Z"
            fill="black"
          />
        </g>
        <defs>
          <clipPath id="clip0">
            <rect width="12" height="12" fill="white" />
          </clipPath>
        </defs>
      </g>
    </svg>
  );
};

export default memo(TelegramSvg);
