import { FC, memo } from "react";

interface Props {
  title?: string;
  link_repo?: string;
  link_project?: string;
}

const ModalHeader: FC<Props> = ({ title, link_project, link_repo }) => {
  return (
    <header className="modal__header">
      <div className="modal__header--info">
        <div className="modal__name">{title}</div>
      </div>
      <div className="modal__header--btn">
        <a
          href={link_repo}
          target="_blank"
          className="modal__btn--repository"
          rel="noreferrer"
        >
          Репозиторий
        </a>
        <a
          href={link_project}
          target="_blank"
          className="modal__btn--project"
          rel="noreferrer"
        >
          Перейти в проект
        </a>
      </div>
    </header>
  );
};

export default memo(ModalHeader);
