import { FC, memo } from "react";

interface Props {
  title?: string;
  text?: string;
}

const ModalContent: FC<Props> = ({ title, text }) => {
  return (
    <div className="modal__main--content">
      <h3 className="modal__main--header">{title}</h3>
      <p className="modal__main--text">{text}</p>
    </div>
  );
};

export default memo(ModalContent);
