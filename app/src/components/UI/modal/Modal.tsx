import { FC, useRef } from "react";
import clsx from "clsx";
import { useNavigate, useParams } from "react-router-dom";
import ModalHeader from "~/components/UI/modal/ModalHeader";
import ModalContent from "~/components/UI/modal/ModalContent";
import TypeModal from "~/components/UI/Type";
import { SkeletonModal } from "~/components/UI/skeleton/SkeletonGroup";
import { useProjectsReadQuery } from "~/store/api";
import useOnClickOutside from "~/hooks/useOnClickOutside";

const Modal: FC = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { data: card, isLoading } = useProjectsReadQuery({
    id: id !== undefined ? id : "0",
  });
  const modalRef = useRef(null);

  useOnClickOutside(modalRef, () => {
    navigate("/portfolio");
  });

  return (
    <section className={clsx("modal", { mask__active: id !== undefined })}>
      <div className="modal__container">
        <div
          ref={modalRef}
          className={clsx("modal__inner", { active__model: id !== undefined })}
        >
          {isLoading ? (
            <SkeletonModal />
          ) : (
            <>
              <ModalHeader
                title={card?.title}
                link_project={card?.link_project}
                link_repo={card?.link_repo}
              />
              <main className="modal__main">
                <div className="modal__main--left">
                  <div className="modal__slider--photos">
                    <img
                      src={card?.avatar}
                      alt=""
                      className="modal__slider--photo"
                    />
                  </div>
                </div>
                <div className="modal__main--right">
                  <div className="modal__types">
                    {card?.types
                      ? card.types.map((type) => (
                          <TypeModal name={type.name} key={type.id} />
                        ))
                      : ""}
                  </div>
                  <ModalContent title="Описание" text={card?.description} />
                  <ModalContent title="Технологии" text={card?.technologies} />
                  <ModalContent title="Инструменты" text={card?.tools} />
                </div>
              </main>
            </>
          )}
        </div>
      </div>
    </section>
  );
};

export default Modal;
