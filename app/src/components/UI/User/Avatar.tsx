import { FC, memo } from "react";
import face from "~/static/images/face.jpg";

const Avatar: FC = () => {
  return (
    <div className="user__info--avatar">
      <img className="user__info--photo" src={face} alt="фото" />
    </div>
  );
};

export default memo(Avatar);
