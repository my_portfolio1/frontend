import { FC, memo } from "react";

interface Props {
  name: string;
  value: string;
}

const UserInfoItem: FC<Props> = ({ name, value }) => {
  return (
    <li className="user__info--item">
      <p>
        <span className="user__info--item-name">{name}:</span>
        {value}
      </p>
    </li>
  );
};

export default memo(UserInfoItem);
