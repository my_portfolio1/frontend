import { FC, memo } from "react";

interface Props {
  username: string;
  specialization: string;
}

const UserName: FC<Props> = ({ username, specialization }) => {
  return (
    <>
      <div className="user__info--username">
        <span className="user__info--name">{username}</span>
        <span className="user__info--active" />
      </div>
      <h4 className="user__info--specialization">{specialization}</h4>
    </>
  );
};

export default memo(UserName);
