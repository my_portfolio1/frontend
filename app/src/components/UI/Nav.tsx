import { FC, useRef, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import clsx from "clsx";
import HomeSvg from "~/components/UI/svg/HomeSvg";
import TelegramSvg from "~/components/UI/svg/TelegramSvg";
import MenuSvg from "~/components/UI/svg/MenuSvg";
import { CONTACT, PORTFOLIO } from "~/constants/route";
import useOnClickOutside from "~/hooks/useOnClickOutside";

const Nav: FC = () => {
  const { pathname } = useLocation();
  const [active, setActive] = useState(false);
  const navRef = useRef(null);

  useOnClickOutside(navRef, () => setActive(false));

  const clickMenu = () => {
    setActive(!active);
  };

  return (
    <div className="nav__block">
      <Link to="/" className="nav__home">
        <HomeSvg />
      </Link>
      <nav className="nav">
        <Link to={PORTFOLIO} className="notext-decoration">
          <span className="nav__link notext-decoration">
            <span
              className={clsx({ active__nav: pathname === `/${PORTFOLIO}` })}
            >
              Портфолио
            </span>
          </span>
        </Link>

        <Link to={CONTACT} className="notext-decoration">
          <span className="nav__link notext-decoration">
            <span className={clsx({ active__nav: pathname === `/${CONTACT}` })}>
              Контакты
            </span>
          </span>
        </Link>
      </nav>
      <a
        href="https://t.me/dmitrymoonchop"
        target="_blank"
        className="nav__telegram"
        rel="noreferrer"
      >
        <span className="nav__telegram-text">Написать</span>
        <span className="nav__telegram-circle">
          <TelegramSvg />
        </span>
      </a>
      <div ref={navRef}>
        <button type="button" className="nav__menu" onClick={clickMenu}>
          <MenuSvg />
        </button>
        <div
          ref={navRef}
          className={clsx("nav_mobile-menu", { active__menu: active })}
        >
          <Link
            to={PORTFOLIO}
            className="notext-decoration"
            onClick={clickMenu}
          >
            <div className="nav__mobile-link notext-decoration">
              <span
                className={clsx({ active__nav: pathname === `/${PORTFOLIO}` })}
              >
                Портфолио
              </span>
            </div>
          </Link>
          <Link to={CONTACT} className="notext-decoration" onClick={clickMenu}>
            <div className="nav__mobile-link notext-decoration">
              <span
                className={clsx({ active__nav: pathname === `/${CONTACT}` })}
              >
                Контакты
              </span>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Nav;
