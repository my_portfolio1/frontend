import { FC } from "react";
import imageFace from "~/static/images/face.jpg";

interface Props {
  active: boolean;
}

const Preloader: FC<Props> = ({ active }) => {
  if (!active) return null;
  return (
    <div className="d">
      <div className="preloader">
        <div className="load" />
      </div>
      <div className="loader__photo">
        <img src={imageFace} className="loader__avatar" alt="" />
      </div>
    </div>
  );
};

export default Preloader;
