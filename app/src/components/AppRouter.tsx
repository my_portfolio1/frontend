import { Navigate, Route, Routes } from "react-router-dom";
import MainLayout from "~/layouts/MainLayout";
import Home from "~/pages/Home";
import PageNotFound from "~/pages/PageNotFound";
import {
  HOME,
  ERROR_404,
  PORTFOLIO,
  CONTACT,
  PORTFOLIO_ITEM,
} from "~/constants/route";
import Portfolio from "~/pages/Portfolio";
import Contact from "~/pages/Contact";

function AppRouter() {
  return (
    <Routes>
      <Route path={HOME} element={<MainLayout />}>
        <Route path="*" element={<Navigate to={ERROR_404} />} />
        <Route index element={<Home />} />
        <Route path={PORTFOLIO} element={<Portfolio />}>
          <Route path={PORTFOLIO_ITEM} element={<Portfolio />} />
        </Route>
        <Route path={CONTACT} element={<Contact />} />
        <Route path={ERROR_404} element={<PageNotFound />} />
      </Route>
    </Routes>
  );
}

export default AppRouter;
