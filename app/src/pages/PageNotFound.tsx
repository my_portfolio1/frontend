import { FC } from "react";
import { Link } from "react-router-dom";

const PageNotFound: FC = () => {
  return (
    <>
      <h1>404 - Page Not Found</h1>
      <Link to="/">
        <span>Go back home</span>
      </Link>
    </>
  );
};

export default PageNotFound;
