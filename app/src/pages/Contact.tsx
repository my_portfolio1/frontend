import { FC } from "react";
import PageBlock from "~/components/UI/PageBlock";
import ContactItem from "~/components/Contact/ContactItem";
import EmailSvg from "~/components/UI/svg/EmailSvg";
import TelegramContactSvg from "~/components/UI/svg/TelegramContactSvg";

const Contact: FC = () => {
  return (
    <div className="home">
      <PageBlock title="Контакты" items position>
        <ContactItem
          title="Email"
          link="mailto:dmitry.itis.gerasimov@gmail.com"
          text="dmitry-dev.front@yandex.ru"
        >
          <EmailSvg />
        </ContactItem>
        <ContactItem
          title="Telegram"
          link="https://t.me/dmitrymoonchop"
          text="@dmitrymoonchop"
        >
          <TelegramContactSvg />
        </ContactItem>
      </PageBlock>
    </div>
  );
};

export default Contact;
