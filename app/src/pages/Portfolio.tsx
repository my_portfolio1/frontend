import { FC } from "react";
import PageBlock from "~/components/UI/PageBlock";
import Cards from "~/components/UI/cards/Cards";
import Modal from "~/components/UI/modal/Modal";

const Portfolio: FC = () => {
  return (
    <>
      <div className="home">
        <PageBlock title="Портфолио">
          <Cards />
        </PageBlock>
      </div>
      <Modal />
    </>
  );
};

export default Portfolio;
