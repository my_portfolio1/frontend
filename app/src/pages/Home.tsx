import { FC } from "react";
import PageBlock from "~/components/UI/PageBlock";
import HomeBlockItem from "~/components/Home/HomeBlockItem";
import BackendSvg from "~/components/UI/svg/BackendSvg";
import TechSvg from "~/components/UI/svg/TechSvg";
import TechInfoItem from "~/components/Home/Tech/TechInfoItem";
import TechInfoItemName from "~/components/Home/Tech/TechInfoItemName";

const Home: FC = () => {
  return (
    <div className="home">
      <PageBlock title="Обо мне">
        <p className="home__about--text">
          Около 2-ух лет занимаюсь разработкой web приложений, и в целом
          основными направлениями в программировании, такими как парсеры данных,
          frontend, backend. Во время выполнения работы соблюдаю три главных
          правила:
          <br />
          1) всегда нахожусь на связи
          <br />
          2) в работе слежу за качеством не только визуальным, но и
          техническим(codestyle)
          <br />
          3) соблюдаю сроки, выставленные заказчиком
        </p>
        <p className="home__about--text">
          Люблю узнавать и изучать новые направления по своей специальности, а
          также участвовать в интересных, необычных проектах. Опыт работы в
          команде имеется.
        </p>
      </PageBlock>

      <PageBlock darkBackground items title="Мои услуги">
        <HomeBlockItem
          title="Frontend"
          text="Опыт около 2-ух лет.
              Основные технологии: Vue, Nuxt, React, Next, JavaScript, Typescript"
        >
          <BackendSvg />
        </HomeBlockItem>
        <HomeBlockItem
          title="Сбор данных"
          text="Опыт около 2-ух лет.
              Основные технологии:
              Python3, Selenium"
        >
          <BackendSvg />
        </HomeBlockItem>
        <HomeBlockItem
          title="Backend"
          text="Опыт около 2-ух лет.
              Основные технологии:
              Python3, Django"
        >
          <BackendSvg />
        </HomeBlockItem>
        <HomeBlockItem
          title="Базы данных"
          text="Базы данных, с которыми работаю:
              MySql/Postgres"
        >
          <BackendSvg />
        </HomeBlockItem>
        <HomeBlockItem
          title="DevOps"
          text="Основные технологии:
              Docker, Ansible, Gitlab CI"
        >
          <BackendSvg />
        </HomeBlockItem>
      </PageBlock>
      <PageBlock title="Технологии">
        <div className="home__body--circle">
          <TechSvg />
        </div>
        <ul className="tech__info--items">
          <li className="tech__info--first" />
          <TechInfoItem title="Frontend">
            <TechInfoItemName
              name="Языки"
              text=" Html 5, Css 3, Less,Sass/Scss, JavaScript(ES6), Typescript"
            />
            <TechInfoItemName
              name="Библиотеки/ Фреймворки"
              text=" VueJs, Vuex, React, Nuxt, Next, WebSockets Api, Jquery, Redux Toolkit, React Query, RTK Query, Rechart"
            />
            <TechInfoItemName name="Сборки" text="webpack, gulp" />
          </TechInfoItem>

          <TechInfoItem title="Backend">
            <TechInfoItemName name="Языки" text="Python3" />
            <TechInfoItemName
              name="Библиотеки/ Фреймворки"
              text=" Django, DjangoRestFramework"
            />
            <TechInfoItemName name="Базы данных" text="mysql, postgres" />
          </TechInfoItem>

          <TechInfoItem title="Сбор данных">
            <TechInfoItemName
              name="Технологии"
              text=" Python3, Selenium, BeautifulSoup4"
            />
          </TechInfoItem>

          <TechInfoItem title="DevOps">
            <TechInfoItemName
              name="Инструменты"
              text=" Docker, Gitlab CI, Ansible, Git"
            />
          </TechInfoItem>
        </ul>
      </PageBlock>
    </div>
  );
};

export default Home;
